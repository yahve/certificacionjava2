<!DOCTYPE html>
<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OCA - Preparación del examen de certificación Java 1Z0-808</title>
       <link href="http://cetys.hol.es/img/OCA_LOGO.png" rel="apple-touch-icon-precomposed" sizes="114x114">
        <link href="http://cetys.hol.es/img/OCA_LOGO.png" rel="shortcut icon">
        
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">

</head>

<body id="page-top" class="index">
    
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom affix-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#"><img src="img/OCA_LOGO.png" style="width:100px;" onclick="cargaModalLogin();"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="#" onclick="cargaModalLogin();">Oracle Certified Associate JAVA SE8</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#" onclick="cargaModalLogin();">Entrar</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

        <div id="central" >
        <!-- Header -->
        <header style="background:#18BC9C;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <img class="img-responsive" style="width:300px;" src="img/d1i3.png" alt="" onclick="cargaModalLogin();">
                        <div class="intro-text">
                            <span class="name">CERTIFICATE!</span>
                            <hr class="star-light">
                            <span class="skills">PREPARACIÓN EXAMEN CERTIFICACIÓN 1Z0-808</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">


            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright © DAM 2017
                        </div>
                    </div>
                </div>
            </div>
        </footer>



        <!--    el modal para el login            -->
        <style>

            @media (min-width:768px){ 
            .largoModal{
                width: 20%;
            }
            }

            @media (max-width:768px){
            .largoModal{
                width: 90%;
            }  
            }
        </style>  

     <div id="loginModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg largoModal" style="">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" >Inicia Sesión</h4>
          </div>
          <div class="modal-body">
              <div class="row" >
                  <div class="col-xs-12">

                        <input type="text" placeholder="Usuario" id="usuario" style="width: 100%;"
                               data-validation="length alphanumeric" 
                                data-validation-length="3-12" 
                                data-validation-error-msg="El nombre de usuario tiene que tener entre 3 y 12 caracteres alfanumericos "
                                data-validation="required"
                               > </input>
                        <br><br>
                        <input id="pass" type="password"  placeholder="Contraseña"  style="width: 100%;"
                               data-validation="required" 
                               data-validation-error-msg="La contraseña no puede estar en blanco"
                               > </input>
                        <br><br>
                        <button class="btn btn-primary btn-block" onclick="iniciaSesion();"> Login <span class="glyphicon glyphicon-triangle-right"></span></button>

                  </div>
                  <div class="col-xs-5">
                      <div id="observacionesLista"></div>
                     <div id="observacionesListaOculta" hidden></div> <!--  -->
                  </div>

              </div>

          </div>
          <div class="modal-footer">
           </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    </div>

    <!-- jQuery -->
    <script src="js/jquery-3.1.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
      <!-- Form Validator -->
      <script src="js/jqueryFormValidator.js"></script>  

    <script>
        function cargaModalLogin(){
            $('#loginModal').modal('show');
        }
     //form validator
    $.validate();
    
        function iniciaSesion(){
            $('.modal-backdrop').hide();
            var _usuario = $('#usuario').val();
            var _pass = $('#pass').val();
            $('#central').load('login.php', {
                usuario: _usuario,
                pass: _pass
            });
        } 
        function cargTemp(){
            $('#temp').load('temporizador.php');
        }
    </script>



</body></html>
