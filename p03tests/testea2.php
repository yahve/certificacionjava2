<!--La carga de la hoja de estilos y del js luego habrá que quitarla cuando esto esté en el marco final-->
<!--<html lang="es">
    <head>
        <link href="../css/bootstrap.css" rel="stylesheet">
    </head>-->
    <?php
//incluyo la función para conectarme a la BBDD
    //include('../funciones.php');
    $mysqli = conectaLi();
    
 function convierteDeComas($listaRespuestas){
     return explode( ",", $listaRespuestas);
 }
 
//esto luego lo cambiamos por el usuario que esté logeado
    $id_usuario = 1;
    $test_elegido = 1;
    
    $query_random_enun = $mysqli->query("SELECT * FROM pregunta ");
    $num_preguntas = $query_random_enun->num_rows;

//guardo todas las respuestas en un array porque es más fácil trabajar luego con javascript
    $preguntas = array();
    for ($a = 0; $a < $num_preguntas; $a++) {
        $sqlrow = $query_random_enun->fetch_array();
        $id_pregunta = $sqlrow['Id_pregunta'];
        $preguntas [$id_pregunta][0] = $sqlrow['Enunciado'];
        $preguntas [$id_pregunta][1] = $sqlrow['R1'];
        $preguntas [$id_pregunta][2] = $sqlrow['R2'];
        $preguntas [$id_pregunta][3] = $sqlrow['R3'];
        $preguntas [$id_pregunta][4] = $sqlrow['R4'];
        $preguntas [$id_pregunta][5] = $sqlrow['R5'];
        $preguntas [$id_pregunta][6] = $sqlrow['R6'];
        $preguntas [$id_pregunta][7] = $id_pregunta;
    }

    //guardo las respuestas en un array, para luego ir actualizando la BBDD
    
    $respuestas = array();
    $query_tests = $mysqli->query("SELECT * FROM TestsRealizados where id_test = '$test_elegido' ");
    $sqlrow = $query_tests->fetch_array();
    //obtiene la lista de respuestas separadas por comas
    $listaRespuestas = $sqlrow['respuestas_dadas'];
    //convierte la lista de respuestas a array
    $respuestas = convierteDeComas($listaRespuestas);
    //lee las preguntas concretas del test y las guarda en una lista
    $listaPreguntas = convierteDeComas($sqlrow['preguntas_elegidas']);
    //print_r( $listaPreguntas);
    $preguntasElegidas = array();
    for ($a=0; $a<count($listaPreguntas)-1; $a++){
        $preguntasElegidas[$a] = $preguntas[$listaPreguntas[$a]];
    }
    
    
    
    //a esta función le pasas la respuesta que quieres poner con php en la pantalla, y el número de respuesta
    //lo que hace es escribir el texto de la pregunta en la fila correspondiente
    function muestraRespuesta($respuesta, $numRespuesta) {
        if ($respuesta != "") {
            echo '<tr><td style="padding:5px; padding-left:30px;" id="respuesta' . $numRespuesta . '"><p><input type=checkbox id="check' . $numRespuesta . '">  ' . $respuesta . '</p></td></tr>';
        }
    }
    
    
    ?>
<style>
    #temp{
        
    }
    tr {
width: 100%;
display: inline-table;
table-layout: fixed;
}
@media (min-width:768px){ 
    table{
     height:600px;              
     display: -moz-groupbox;    
    }
    tbody{
      overflow-y: scroll;      
      height: 600px;           
      width: 100%;
      position: absolute;
    }
}

@media (max-width:768px){
     table{
     height:1200px;              
     display: -moz-groupbox;    
    }
    tbody{
      overflow-y: scroll;      
      height: 400px;           
      width: 100%;
      position: absolute;
    }   
}
.dCode{
    font-family: 'Courier New';
}

.dNoWrap{
    white-space: pre;
}
</style>
<?php 
    //require ('temporizador.php');
?>

<div style="padding-top: 100px;">
    <div class="container-fluid">
       
        <div class="row" >
            
            
        <table class="table  table-condensed table-bordered" style="background-color: #999; color: whitesmoke;">
            <tr><td  style="padding:30px;"> <span id="numeroPregunta"> Pregunta 1 de <?php echo count($listaPreguntas); ?> </span>   
                    <button id="siguiente" class="btn btn-info" onclick="siguiente();">SIGUIENTE</button> 
                    <button class="btn btn-warning" id="tiempo"></button>
                    <button class="btn btn-primary" id="tiempoTotal"></button>
                </td></tr>
            <?php
            $a = 0;
            echo '<tr><td  id="pregunta" style="padding:30px; ">' . $preguntasElegidas [$a][0] . '</td></tr>';
            for ($i = 1; $i < 7; $i++) {
                muestraRespuesta($preguntasElegidas [$a][$i], $i);
            }
           
            ?>
           <tr><td  style="padding:30px;"></td></tr> 
        </table>
        </div>
    </div>
</div>
    <div id="guarda"></div>


<!--esta carga de script de jquery y bootstrap hay que borrarla cuando pongamos este código dentro del marco final-->
<!--    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>-->
    <script>
            var preguntas = <?php echo json_encode($preguntasElegidas); ?>;
            var respuestas = <?php echo json_encode($respuestas); ?>;
            var max_preguntas = <?php echo json_encode($num_preguntas); ?>;
            var id_test = <?php echo json_encode($test_elegido); ?>;
            var num_pregunta = 0;
            var numeroDePreguntas = <?php echo count($listaPreguntas); ?>;
            var tiempo_transcurrido;
            var tiempo_total = 5400;
            var segundos = 0;
        
        function reseteaTemporizador(){
            segundos = 0;
            $('#tiempo').html("0");
            clearInterval(tiempo_transcurrido);
            tiempo_transcurrido = setInterval(function(){
                segundos ++;
                $('#tiempo').html("     "+segundos+"  ");
            }, 1000);
        }
        
        $( document ).ready(function() {
            console.log(preguntas);
            escribePreguntas();
            reseteaTemporizador();
            tiempo_atras = setInterval(function(){
                tiempo_total --;
                $('#tiempoTotal').html("     "+tiempo_total+"  ");
            }, 1000);
        });
        

            //La función siguiente se encarga de manejar lo que pasa cuando se pulsa el botón siguiente
            //tiene que: 
            
            //  guardar las respuestas que ha dado el usuario en la base de datos
            //  //  cambiar la pregunta y si están en blanco no ponerlas
            //  chequear si ha terminado la partida, si quiere pausar, recuperar un test empezado etc
            
        function siguiente() {
            
            reseteaTemporizador();
            if (num_pregunta < numeroDePreguntas -1){

                //lee los ticks de las preguntas actuales y los inserta en la BBDD
                //el peazo código de actualizaRespuestasTest es de Hector
                var r = '';
                for (var i = 1; i < 7; i++) {
                    if(leeTick(i)){ r = r + '1'; }
                    else {r = r + '0';}
                }
                respuestas[num_pregunta] = r;
                var textoRespuestas = respuestas.toString()
                $('#guarda').load('p03tests/actualizaRespuestasTest.php',{
                    id_test : id_test,
                    respuestas_dadas : textoRespuestas
                });
                num_pregunta++;
                escribePreguntas();
                //console.log(respuestas);
            }
 
                
        }
            
            
            
            
            
        function escribePreguntas(){
                
            //pone la pregunta correspondiente
            $('#pregunta').html( preguntas[num_pregunta][0] );
            $('#numeroPregunta').html( "Pregunta " + (num_pregunta+1) + " de " + numeroDePreguntas + "  id: " + preguntas[num_pregunta][7]+ "  ");
            //pone cada respuesta en su sitio. Si está en blanco, lo borra por si hubiera otra respuesta anterior
            for (var i = 1; i < 7; i++) {
                if (preguntas[num_pregunta][i] != "") {
                    $('#respuesta' + i).html('<tr><td  id="respuesta' + i + '"><span><input type=checkbox id="check' + i + '">  ' + preguntas[num_pregunta][i] + '</span></td></tr>');
                }
                else {
                    $('#respuesta' + i).html('');
                }
                if (respuestas[num_pregunta][i-1] == '1' ) {
                    marcaTicks(i);
                }

            }
        }        
            
        function marcaTicks(i){
            $('#check' + i).prop('checked', true);
        }

        function leeTick(i){
            return ($('#check' + i).prop('checked'));
        }
            
            
            
            

    </script>


