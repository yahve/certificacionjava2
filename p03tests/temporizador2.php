
<style>
.containerr {
  text-align: center;
}

input {
  width: 30px;
}

.inputs {
  margin-top: 30px;
}

#timerr {
  border-radius: 20px;
  margin-top: 50px;
  font-size: 50px;
  max-width: 220px;
}

.goo {  
  color: white;
  background-color: cyan;  
}

.stopp {  
  color: white;
  background-color: darkred;
}
            
</style>
   
 
<div class="containerr">
  <div id="timerr" class="containerr">
  </div>
</div>
<script>
    
     $( document ).ready(function() {   
        
     resetTimer();
  var sessionHours = 0;
  var sessionMins = 0;
  var sessionSecs = 0;
//  var breakHours = 0;
//  var breakMins = 0;
//  var breakSecs = 0;
  var startCount = 0;
  var startBreakCount = 0;
  var count = 0;
  var breakCount = 0;
  var counter;
  var breakCounter;

  $("#stopp").hide();
  getInput();
  displayTime(+sessionHours * 3600 + +sessionMins * 60 + +sessionSecs);

  $("inputt").keyup(function() {    
    getInput();
    displayTime(+sessionHours * 3600 + +sessionMins * 60 + +sessionSecs);
  });

  
    getInput();
    startCount = +sessionHours * 3600 + +sessionMins * 60 + +sessionSecs;
//    startBreakCount = +breakHours * 3600 + +breakMins * 60 + +breakSecs;
    count = startCount;
    displayTime(count);
    if ( startCount === 0) {
      $("inputt").prop('disabled', true);
      $("#startt").hide();
      $("#timerr").addClass("goo"); 
      counter = setInterval(timer, 1000);
    }
    if ( breakCount === 0) {        
      $("#stopp").show();
      $("#stopp").text('Start');
    }







  
  
  $("#stopp").click(function() {
    $("inputt").prop('disabled', false);
    $(".inputs").show();
    $(".inputs-hidden").hide();
    $("#startt").show();
    $("#stopp").hide();
    clearInterval(counter);
    count = startCount;
    clearInterval(breakCounter);
    breakCount = startBreakCount;
    displayTime(count);
    $("#timerr").removeClass("goo");
      $("#timerr").removeClass("stopp");
  });

  function timer() {
    if (count === startCount) {
      $("#timerr").addClass("goo");
      $("#timerr").removeClass("stopp");
    }
    count++;
    displayTime(count);
    if (count === 0) {
      clearInterval(counter);
      count = startCount;
      breakCount = startBreakCount;
      //breakCounter = setInterval(breakTimer, 1000);
      if (breakCount === startBreakCount) {
        $("#timerr").addClass("stopp");
        $("#timerr").removeClass("goo");
      }
      return;
    }
  }
  function resetTimer(){
      sessionHours = 0;
      sessionMins = 00;
      sessionSecs = 00;
  }

  function breakTimer() {
    
//    if (breakCount === startBreakCount) {
//      $("#timer").addClass("stop");
//      $("#timer").removeClass("go");
//    }
    
//    breakCount;
//    displayTime(breakCount);
//    if (breakCount <= 0) {
//      clearInterval(breakCounter);
//      breakCount = startBreakCount;
//      counter = setInterval(timer, 1000);
//      return;
//    }
  }

  function getInput() {
    sessionHours = 0;
    sessionMins = 00;
    sessionSecs = 00;
//    breakHours = 1;//$("#bh").val();
//    breakMins = 00;//$("#bm").val();
//    breakSecs = 10;//$("#bs").val();
  }

  function displayTime(seconds) {
    var h = 0;
    var m = 0;
    var s = 0;

    h = Math.floor(seconds / 3600);
    seconds -= h * 3600;
    m = Math.floor(seconds / 60);
    seconds -= m * 60;
    s = Math.floor(seconds);

    h = ("0" + h).slice(-2);
    m = ("0" + m).slice(-2);

    if ((h > 0) || (m > 0))
      s = ("0" + s).slice(-2);
    if (h === 0)
      h = "";
    else h += ":";
    if ((m === 0) && (h === 0))
      m = "";
    else m += ":";
    $("#timerr").html(h + m + s);
    return;
  };
});
</script>
