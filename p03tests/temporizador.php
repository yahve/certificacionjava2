

<style>
.container {
  text-align: center;
}

input {
  width: 30px;
}

.inputs {
  margin-top: 30px;
}

#timer {
  border-radius: 20px;
  margin-top: 50px;
  font-size: 50px;
  max-width: 220px;
}

.go {  
  color: white;
  background-color: darkgrey;  
}

.stop {  
  color: white;
  background-color: darkred;
}
            
</style>




  <br><br><br><br><br><br>
<div class="container" id="tiempo">
  <div>
    <button id="start">Aceptar</button>
    <button id="stop">Stop</button>
  </div>
  <div id="timer" class="container">
  </div>
</div>





    
   
    
    
    


       


<script>
        
        
        $(document).ready(function() {

  var sessionHours = 0;
  var sessionMins = 0;
  var sessionSecs = 0;
//  var breakHours = 0;
//  var breakMins = 0;
//  var breakSecs = 0;
  var startCount = 0;
  var startBreakCount = 0;
  var count = 0;
  var breakCount = 0;
  var counter;
  var breakCounter;

  $("#stop").hide();
  getInput();
  displayTime(+sessionHours * 3600 + +sessionMins * 60 + +sessionSecs);

  $("input").keyup(function() {    
    getInput();
    displayTime(+sessionHours * 3600 + +sessionMins * 60 + +sessionSecs);
  });

  $("#start").click(function() {
    getInput();
    startCount = +sessionHours * 3600 + +sessionMins * 60 + +sessionSecs;
//    startBreakCount = +breakHours * 3600 + +breakMins * 60 + +breakSecs;
    count = startCount;
    displayTime(count);
    if ( startCount > 0) {
      $("input").prop('disabled', true);
      $("#start").hide();
      $("#timer").addClass("go"); 
      counter = setInterval(timer, 1000);
    }
//    if ( breakCount === 0) {        
//      $("#stop").show();
//      $("#stop").text('Start');
//    }
  });
  
  $("#stop").click(function() {
    $("input").prop('disabled', false);
    $(".inputs").show();
    $(".inputs-hidden").hide();
    $("#start").show();
    $("#stop").hide();
    clearInterval(counter);
    count = startCount;
    clearInterval(breakCounter);
    breakCount = startBreakCount;
    displayTime(count);
    $("#timer").removeClass("go");
      $("#timer").removeClass("stop");
  });

  function timer() {
    if (count === startCount) {
      $("#timer").addClass("go");
      $("#timer").removeClass("stop");
    }
    count--;
    displayTime(count);
    if (count <= 0) {
      clearInterval(counter);
      count = startCount;
      breakCount = startBreakCount;
      //breakCounter = setInterval(breakTimer, 1000);
      if (breakCount === startBreakCount) {
        $("#timer").addClass("stop");
        $("#timer").removeClass("go");
      }
      return;
    }
  }

  function breakTimer() {
    
//    if (breakCount === startBreakCount) {
//      $("#timer").addClass("stop");
//      $("#timer").removeClass("go");
//    }
    
//    breakCount;
//    displayTime(breakCount);
//    if (breakCount <= 0) {
//      clearInterval(breakCounter);
//      breakCount = startBreakCount;
//      counter = setInterval(timer, 1000);
//      return;
//    }
  }

  function getInput() {
    sessionHours = 1;
    sessionMins = 30;
    sessionSecs = 00;
//    breakHours = 1;//$("#bh").val();
//    breakMins = 00;//$("#bm").val();
//    breakSecs = 10;//$("#bs").val();
  }






  function displayTime(seconds) {
    var h = 0;
    var m = 0;
    var s = 0;

    h = Math.floor(seconds / 3600);
    seconds -= h * 3600;
    m = Math.floor(seconds / 60);
    seconds -= m * 60;
    s = Math.floor(seconds);

    h = ("0" + h).slice(-2);
    m = ("0" + m).slice(-2);

    if ((h > 0) || (m > 0))
      s = ("0" + s).slice(-2);
    if (h === 0)
      h = "";
    else h += ":";
    if ((m === 0) && (h === 0))
      m = "";
    else m += ":";
    $("#timer").html(h + m + s);
    return;
  };
});
</script>


